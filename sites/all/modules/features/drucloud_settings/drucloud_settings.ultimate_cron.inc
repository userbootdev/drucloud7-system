<?php
/**
 * @file
 * drucloud_settings.ultimate_cron.inc
 */

/**
 * Implements hook_default_ultimate_cron_job().
 */
function drucloud_settings_default_ultimate_cron_job() {
  $export = array();

  $job = new stdClass();
  $job->disabled = FALSE; /* Edit this to true to make a default job disabled initially */
  $job->api_version = 3;
  $job->name = 'queue_social_stats_update_stats';
  $job->title = 'Queue: social_stats_update_stats';
  $job->settings = array(
    'settings' => array(
      'queue' => array(
        'throttle' => 0,
      ),
      'general' => array(),
      'poorman' => array(),
    ),
    'scheduler' => array(
      'name' => '',
      'simple' => array(
        'rules' => array(
          0 => '*/30+@ * * * *',
        ),
      ),
    ),
    'launcher' => array(
      'name' => '',
      'serial' => array(),
    ),
    'logger' => array(
      'name' => '',
      'database' => array(),
    ),
  );
  $export['queue_social_stats_update_stats'] = $job;

  $job = new stdClass();
  $job->disabled = FALSE; /* Edit this to true to make a default job disabled initially */
  $job->api_version = 3;
  $job->name = 'social_stats_cron';
  $job->title = 'Default cron handler';
  $job->settings = array(
    'scheduler' => array(
      'name' => '',
      'simple' => array(
        'rules' => array(
          0 => '0+@ 0 * * *',
        ),
      ),
    ),
    'launcher' => array(
      'name' => '',
      'serial' => array(),
    ),
    'logger' => array(
      'name' => '',
      'database' => array(),
    ),
    'settings' => array(
      'queue' => array(),
      'general' => array(),
      'poorman' => array(),
    ),
  );
  $export['social_stats_cron'] = $job;

  $job = new stdClass();
  $job->disabled = FALSE; /* Edit this to true to make a default job disabled initially */
  $job->api_version = 3;
  $job->name = 'xmlsitemap_cron';
  $job->title = 'Default cron handler';
  $job->settings = array(
    'scheduler' => array(
      'name' => '',
      'simple' => array(
        'rules' => array(
          0 => '0+@ 0 * * *',
        ),
      ),
    ),
    'launcher' => array(
      'name' => '',
      'serial' => array(),
    ),
    'logger' => array(
      'name' => '',
      'database' => array(),
    ),
    'settings' => array(
      'queue' => array(),
      'general' => array(),
      'poorman' => array(),
    ),
  );
  $export['xmlsitemap_cron'] = $job;

  return $export;
}
