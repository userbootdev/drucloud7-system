<?php
/**
 * @file
 * drucloud_tagroll.entityqueue_default.inc
 */

/**
 * Implements hook_entityqueue_default_queues().
 */
function drucloud_tagroll_entityqueue_default_queues() {
  $export = array();

  $queue = new EntityQueue();
  $queue->disabled = FALSE; /* Edit this to true to make a default queue disabled initially */
  $queue->api_version = 1;
  $queue->name = 'primary_tagroll';
  $queue->label = 'Primary tagroll';
  $queue->language = 'en';
  $queue->handler = 'simple';
  $queue->target_type = 'taxonomy_term';
  $queue->settings = array(
    'target_bundles' => array(
      'tags' => 'tags',
    ),
    'min_size' => '0',
    'max_size' => '0',
  );
  $export['primary_tagroll'] = $queue;

  $queue = new EntityQueue();
  $queue->disabled = FALSE; /* Edit this to true to make a default queue disabled initially */
  $queue->api_version = 1;
  $queue->name = 'secondary_tagroll';
  $queue->label = 'Secondary tagroll';
  $queue->language = 'en';
  $queue->handler = 'simple';
  $queue->target_type = 'taxonomy_term';
  $queue->settings = array(
    'target_bundles' => array(
      'tags' => 'tags',
    ),
    'min_size' => '0',
    'max_size' => '0',
  );
  $export['secondary_tagroll'] = $queue;

  return $export;
}
